<?php
namespace Maksatech\Core;

/**
 * Trait ClassTrait
 * @package Maksatech\Core
 */
trait ClassTrait {

    /**
     * @param string $class
     * @param array $plist
     * @return array
     */
    public static function getClassParents(string $class, array $plist = []): array
    {
        if(!class_exists($class)) {
            return $plist;
        }

        $parent = get_parent_class($class);

        if ($parent) {
            $plist[] = $parent;
            $plist = self::getClassParents($parent, $plist);
        }

        return $plist;
    }

    /**
     * @param string|null $class
     * @param int $level
     * @return string
     */
    public static function getClassNamespace(string $class = NULL, int $level = 1): string
    {
        $namespace = "";

        if(is_null($class))
            $classPaths = explode('\\',get_called_class());
        else
            $classPaths = explode('\\',$class);

        for ($i = 0; $i < count($classPaths)-$level; $i++) {
            if($i > 0)
                $namespace .= "\\";

            $namespace .= $classPaths[$i];
        }

        return $namespace;
    }

}