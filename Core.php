<?php
namespace Maksatech\Core;

use Dotenv\Dotenv;
use Exception;

/**
 * Class Core
 * @package Maksatech\Core
 */
abstract class Core
{
    use ClassTrait;
    use StringTrait;

    /**
     * @var string
     */
    protected static string $configNamespace = 'core';

    /**
     * @var string
     */
    protected static string $configDir = '';

    /**
     * @var string
     */
    protected static string $configName = 'core';

    /**
     * @var Config[]
     */
    protected static array $configCaches = [];


    /**
     * Core constructor.
     */
    function __construct()
    {
        self::loadEnv();
    }

    /**
     * @param bool $load
     * @param bool $caching
     * @return Config
     * @throws Exception
     */
    public static function getConfig(bool $load = false, bool $caching = false): Config
    {
        if(!$caching || !array_key_exists(static::class,static::$configCaches) || ($load !== static::$configCaches[static::class]->isLoaded())) {
            static::$configCaches[static::class] = new Config(static::$configNamespace,static::$configDir,static::$configName);
            if($load)
                static::$configCaches[static::class]->load();
        }

        return static::$configCaches[static::class];
    }

    /**
     * @throws Exception
     */
    public static function cacheInit()
    {
        $config = self::getConfig(true);

        if($config->has('storage_dir')) {
            if(!file_exists(self::storageDirPath().'/cache'))
                mkdir(self::storageDirPath().'/cache', 0777, TRUE);

            file_put_contents(self::storageDirPath().'/cache/core',self::generateString(12));
        }
    }

    /**
     * @return string
     * @throws Exception
     */
    public static function getCacheString(): string
    {
        if(file_exists(self::storageDirPath().'/cache/core'))
            return file_get_contents(self::storageDirPath().'/cache/core');
        else
            return "";
    }

    function __destruct()
    {

    }

    /**
     * @return false|string
     */
    public static function systemRootDirPath()
    {
        return realpath(__DIR__.'/../../..');
    }

    /**
     * @return string
     * @throws Exception
     */
    public static function webRootDirPath(): string
    {
        $config = Core::getConfig(true, true);

        if($config->has('web_root_dir')) {
            return self::systemRootDirPath() . '/' . $config->get('web_root_dir');
        }

        throw new Exception('The configuration file doesn\'t have "web_root_dir" property');
    }

    /**
     * @return string
     * @throws Exception
     */
    public static function storageDirPath(): string
    {
        $config = Core::getConfig(true, true);

        if($config->has('storage_dir')) {
            return self::systemRootDirPath(). '/' . $config->get('storage_dir');
        }

        throw new Exception('The configuration file doesn\'t have "storage_dir" property');
    }

    public static function loadEnv(): void
    {
        $dotenv = Dotenv::createImmutable(self::systemRootDirPath());
        $dotenv->safeLoad();
    }

    public static function envType(): string
    {
        if(isset($_ENV['APP_ENV'])) {
            return $_ENV['APP_ENV'];
        }

        return 'prod';
    }
}