<?php
namespace Maksatech\Core;

use Composer\Script\Event;
use Composer\Installer\PackageEvent;
use ReflectionException;
use Exception;

/**
 * Class Installer
 * @package Maksatech\Core
 */
abstract class Installer extends Core
{

    use StringTrait;

    /**
     * Installer constructor.
     */
    function __construct()
    {
        parent::__construct();
    }

    function __destruct()
    {
        parent::__destruct();
    }

    /**
     * @param PackageEvent|null $event
     * @throws ReflectionException|Exception
     */
    public static function init(PackageEvent $event = null): void
    {
        $config = self::getConfig();

        if($config->hasFile())
            $config->load();

        $method = new \ReflectionMethod(get_class($config),'put');

        if($method->isProtected() || $method->isPrivate())
            $method->setAccessible(true);

        $method->invokeArgs($config,[[
            'storage_dir' => 'storage',
            'web_root_dir' => 'web',
        ]]);

        self::cacheInit();

        if($config->has('web_root_dir') && !file_exists(self::webRootDirPath()))
            mkdir(self::webRootDirPath(),0755,true);

    }
}