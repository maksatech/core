<?php
namespace Maksatech\Core\Exceptions;

use Exception;
use Throwable;

/**
 * Class MethodNotFoundException
 * @package Maksatech\Core\Exceptions
 */
class MethodNotFoundException extends Exception
{
    /**
     * MethodNotFoundException constructor.
     * @param string $className
     * @param string $methodName
     * @param Throwable|null $previous
     */
    public function __construct(string $className, string $methodName, Throwable $previous = null)
    {
        parent::__construct('Method '. $methodName .' not found in '.$className.' class', 0, $previous);
    }

    public function __destruct()
    {

    }
}