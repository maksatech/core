<?php
namespace Maksatech\Core\Exceptions;

use Exception;
use Throwable;

/**
 * Class ClassNotFoundException
 * @package Maksatech\Core\Exceptions
 */
class ClassNotFoundException extends Exception
{
    /**
     * ClassNotFoundException constructor.
     * @param string $className
     * @param Throwable|null $previous
     */
    public function __construct(string $className, Throwable $previous = null)
    {
        parent::__construct('Class '. $className .' not found', 0, $previous);
    }

    public function __destruct()
    {

    }
}