<?php
namespace Maksatech\Core;

use Exception;

/**
 * Class Config
 * @package Maksatech\Core
 */
class Config extends Core {

    /**
     * @var string
     */
    protected string $namespace;

    /**
     * @var string
     */
    protected string $dir;

    /**
     * @var string
     */
    protected string $name;

    /**
     * @var null|array
     */
    protected $properties;

    /**
     * Config constructor.
     * @param string $namespace
     * @param string $dir
     * @param string $name
     */
    function __construct(string $namespace, string $dir, string $name)
    {
        parent::__construct();
        $this->namespace = $namespace;
        $this->dir = $dir;
        $this->name = $name;
        $this->properties = null;
    }

    function __destruct()
    {
        parent::__destruct();
    }

    /**
     * @return string
     */
    protected function fileDirPath(): string
    {
        return self::systemRootDirPath() . '/' . $this->namespace . '/config'. (mb_strlen($this->dir) > 0?"/":"") . $this->dir;
    }

    /**
     * @return string
     */
    protected function filePath(): string
    {
        return $this->fileDirPath().'/'.$this->name.'.php';
    }

    /**
     * @return bool
     */
    public function hasFile(): bool
    {
        return file_exists($this->filePath());
    }

    /**
     * @param array $properties
     */
    protected function put(array $properties = []): void
    {
        if(!file_exists($this->fileDirPath()))
            mkdir($this->fileDirPath(),0755,TRUE);

        if($this->isLoaded())
            $this->properties = $this->properties+$properties;
        else
            $this->properties = $properties;

        file_put_contents($this->filePath(),"<?php\n\n\$properties = ".var_export($this->properties,true).";\n\n?>");
    }

    /**
     * @return bool
     */
    public function isLoaded(): bool
    {
        return !is_null($this->properties);
    }

    /**
     * @throws Exception
     */
    public function load(): void
    {
        if($this->hasFile()) {
            $properties = [];
            require $this->filePath();
            $this->properties = $properties;
        }
        else
            throw new Exception('The file '.$this->filePath().' does not exist');
    }

    /**
     * @param string $propertyName
     * @return bool
     * @throws Exception
     */
    public function has(string $propertyName): bool
    {
        if($this->isLoaded())
            return array_key_exists($propertyName,$this->properties);

        throw new Exception("Configuration is not loaded");
    }

    /**
     * @param string $propertyName
     * @return mixed
     * @throws Exception
     */
    public function get(string $propertyName)
    {
        if($this->has($propertyName))
            return $this->properties[$propertyName];

        throw new Exception("The configuration doesn't have '".$propertyName."' property");
    }

    /**
     * @return array
     * @throws Exception
     */
    public function getAll(): array
    {
        if($this->isLoaded())
            return $this->properties;

        throw new Exception("Configuration is not loaded");
    }

    /**
     * @return array
     * @throws Exception
     */
    public function getKeys(): array
    {
        if($this->isLoaded())
            return array_keys($this->properties);

        throw new \Exception("Configuration is not loaded");
    }
}