<?php
namespace Maksatech\Core;

/**
 * Trait SetGetTrait
 * @package Maksatech\Core
 */
trait SetGetTrait {

    /**
     * @var array
     */
    protected array $properties = [];

    /**
     * @param $name
     * @return bool
     */
    function __isset($name): bool
    {
        return isset($this->properties[$name]);
    }

    /**
     * @param $name
     * @param $value
     */
    function __set($name, $value): void
    {
        $this->properties[$name] = $value;
    }

    /**
     * @param $name
     * @return mixed
     */
    function __get($name)
    {
        return $this->properties[$name] ?? null;
    }

}