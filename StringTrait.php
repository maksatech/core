<?php
namespace Maksatech\Core;

/**
 * Trait StringTrait
 * @package Maksatech\Core
 */
trait StringTrait {

    /**
     * @param int $length
     * @return string
     */
    public static function generateString(int $length = 1): string
    {
        $symbols = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        return mb_substr(str_shuffle($symbols), 1, $length);
    }

}